#include <bits/stdc++.h>
#include <czmq.h>


using namespace std;
typedef vector<vector<int>> Matrix;
Matrix ma,mb,mc;
int tamReal;

void multiplicar(Matrix ma , Matrix  mb , Matrix  mc){

  int r = ma.size();
  int c = mb[0].size();
  mc.resize(r);

  for (int i = 0; i < r; i++){
    mc[i].resize(c);    
  }
  for (int i = 0; i < r; i++){
    for (int j = 0; j < c; j++){
      for (int k = 0; k < mb.size(); k++){
        mc[i][j] = mc[i][j] + (ma[i][k]* mb[k][j]);        
        cout<<mc[i][j]<<" ";    
      } 
    }
  }

}


void decrip(zmsg_t* msg)
{   
      //MATRIZ A
      int m =atoi(zmsg_popstr(msg));  //Numero Filas de matriz A
      tamReal=m;
      int n =atoi(zmsg_popstr(msg));  
      int element=0; 
      ma.resize(1);

      cout<<"Matriz1 "<<m<<" "<<n<<endl;
      for (int i = 0; i < 1; i++){
        ma[i].resize(n);
        for (int j = 0; j < n; j++){
          element=atoi(zmsg_popstr(msg));
          ma[i][j]=element;
          cout<<ma[i][j]<<" ";
        }
        cout<<endl;
      } 

      //MATRIZ B
      int nn =atoi(zmsg_popstr(msg)); //n Numero filas de matriz B
      int o =atoi(zmsg_popstr(msg)); //o  Numero columnas matriz B
      cout<<"Matriz2 "<<m<<" "<<n<<endl;
      mb.resize(nn);
      for (int i = 0; i < nn; i++){
        mb[i].resize(o);
        for (int j = 0; j < o; j++){
          element=atoi(zmsg_popstr(msg));
          mb[i][j]=element;
          cout<<mb[i][j]<<" ";
        }
        cout<<endl;
      }
}

void crip(zframe_t* clientId,int index,void * server){
  
  zmsg_t* result = zmsg_new();
  zmsg_addstr(result, "answer");
  //--------------------------
  

  zmsg_append(result,&clientId);
  zmsg_addstr(result,to_string(index).c_str()); /* Indice */
   //cout<<"m: "<<ma.size()<<"o "<<mb[0].size()<<endl;

  /*for (int i = 0; i < ma.size(); i++){
    for (int j = 0; j < mb[0].size(); j++)
    {
      cout<<mc[0][0]<<endl;
    }
  }
 */

///INICIO MULTIPLICACION
  int r = ma.size();
  int c = mb[0].size();

  zmsg_addstr(result,to_string(tamReal).c_str()); /* Numero filas de C*/  
  zmsg_addstr(result,to_string(c).c_str()); /* Tamaño O del mensaje*/  

  mc.resize(1);

  for (int i = 0; i < 1; i++){
    mc[i].resize(c);    
  }
  for (int i = 0; i < 1; i++){
    for (int j = 0; j < c; j++){
      for (int k = 0; k < mb.size(); k++){
        mc[i][j] = mc[i][j] + (ma[i][k]* mb[k][j]);
        
      } 
      zmsg_addstr(result,to_string(mc[i][j]).c_str());
    }
  }

///FIN MULTIPLICACION

  zmsg_print(result);
  zmsg_send(&result, server); 
  tamReal=0;
  ma.clear();
  mb.clear();
  mc.clear();


  cout << "End of handling" << endl;
  zmsg_destroy(&result);

}



void handleServerMessage(zmsg_t* msg, void* server) {
  cout << "Handling the following from SERVER" << endl;
  zmsg_print(msg);

  zframe_t* clientId = zmsg_pop(msg);
  int i= atoi(zmsg_popstr(msg)); // Indice

  //zframe_print(clientId,"ID , New msg: ");
  //zmsg_print(msg);  
  decrip(msg);
 // multiplicar(ma,mb,mc); 
  crip(clientId,i,server);
  

}

int main(int argc, char** argv) {
  cout << "Worker agent starting..." << endl;
  if (argc != 1) {
    cerr << "Wrong call\n";
    return 1;
  }

  zctx_t* context = zctx_new();
  void* server = zsocket_new(context, ZMQ_DEALER);

  int c = zsocket_connect(server, "tcp://localhost:5555");
  cout << "connecting to server: " << (c == 0 ? "OK" : "ERROR") << endl;

  // Register the worker with the server
  zmsg_t* regmsg = zmsg_new();
  zmsg_addstr(regmsg, "register");
  zmsg_send(&regmsg, server);
  // zstr_send(server, argv[1]);

  zmq_pollitem_t items[] = {{server, 0, ZMQ_POLLIN, 0}};

  while (true) {
    zmq_poll(items, 1, 10 * ZMQ_POLL_MSEC);
    if (items[0].revents & ZMQ_POLLIN) {
      zmsg_t* msg = zmsg_recv(server);
      handleServerMessage(msg, server);
    }
  }
  zctx_destroy(&context);
  return 0;
}
