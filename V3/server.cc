/****************************************

****************************************/
#include <czmq.h>
#include <bits/stdc++.h>
using namespace std;

typedef vector<vector<int> > Matrix;
Matrix ma,mb,mc;
unordered_map<zframe_t*,priority_queue<pair<int, zmsg_t* > > > build; // Guardar las filas y ordenar en cola de prioridad


typedef vector<zframe_t*>WorkerReferences;
WorkerReferences wr;

//Contruir matrices
void decrip(zmsg_t* msg)
{   
      //MATRIZ A
      int m =atoi(zmsg_popstr(msg));  
      int n =atoi(zmsg_popstr(msg));  
      int element=0; 
      ma.resize(m);

      cout<<"Matriz1 "<<m<<" "<<n<<endl;
      for (int i = 0; i < m; i++){
        ma[i].resize(n);
        for (int j = 0; j < n; j++){
          element=atoi(zmsg_popstr(msg));
          ma[i][j]=element;
          cout<<ma[i][j]<<" ";
        }
        cout<<endl;
      } 

      //MATRIZ B
      m =atoi(zmsg_popstr(msg)); //n
      n =atoi(zmsg_popstr(msg)); //o 
      cout<<"Matriz2 "<<m<<" "<<n<<endl;
      mb.resize(m);
      for (int i = 0; i < m; i++){
        mb[i].resize(n);
        for (int j = 0; j < n; j++){
          element=atoi(zmsg_popstr(msg));
          mb[i][j]=element;
          cout<<mb[i][j]<<" ";
        }
        cout<<endl;
      }  

}

void registerWorker(zframe_t* id){
  zframe_print(id, "Id to add");
  zframe_t* dup = zframe_dup(id);
  zframe_print(dup, "Id copy add");
  wr.push_back(dup);  
}

zframe_t* getWorkerFor(){ 
  int index= rand() % wr.size(); 
  zframe_t* wid = wr[index];  
  cout<<"Indice "<<index<<endl;
  zframe_print(wid,"Worker seleccionado");
   return zframe_dup(wid);
}


/*Funcion que me construye el mensaje para mandarlo al worker*/
void enviador(zframe_t* idC,void* workers){



  int tamA=ma.size(),tamB=mb.size();
  int colA=ma[0].size(),colB=mb[0].size(); //m


  for (int i = 0; i < tamA; i++){

    zmsg_t* subM= zmsg_new(); 
    zframe_t* worker = getWorkerFor();
    zframe_t* id=zframe_dup(idC);
    zmsg_append(subM,&worker);
    zmsg_append(subM,&id);

    zmsg_addstr(subM,to_string(i).c_str());
    zmsg_addstr(subM,to_string(tamA).c_str()); //M Filas
    zmsg_addstr(subM,to_string(colA).c_str()); //N Columnas

    for (int j = 0; j < colA; j++){
      zmsg_addstr(subM,to_string(ma[i][j]).c_str());
    }
    
    //for para empaquetar la matriz B

    zmsg_addstr(subM,to_string(tamB).c_str()); //n
    zmsg_addstr(subM,to_string(colB).c_str()); //o
   // zmsg_print(subM);

    //Agrega al mensaje toda la matriz B.
    for (int k = 0; k <tamB ;k++){
      for (int l = 0; l < colB; l++){
         zmsg_addstr(subM,to_string(mb[k][l]).c_str()); //n
      }
    }
    zmsg_print(subM);
    zmsg_send(&subM, workers);
   
    zframe_destroy(&worker);

  }

}
void recibidor(zmsg_t* msg,void* clients){
  cout<<"Hola Recibidor"<<endl;
  zmsg_print(msg);
  
  int index= atoi(zmsg_popstr(msg));
  zframe_t* idC=zmsg_pop(msg);
  int m=atoi(zmsg_popstr(msg));
  int o=atoi(zmsg_popstr(msg));
  
  zframe_t* dup=zframe_dup(idC);
  zframe_t* dup2=zframe_dup(idC);
  zframe_print(dup,"Id del cliente");
  zmsg_print(msg);
  build[dup].push(make_pair(index,msg));
  int tamCola=build[zframe_dup(dup)].size();
  cout<<"Tamaño de la cola de prioridad "<<tamCola<<endl;

  zmsg_print(build[dup2].top().second);

  cout<<"PRUEBA\n"; 
 if(tamCola=m){
      cout<<"NO encuentro el error"<<endl;
      zframe_t* id=zframe_dup(idC);
      zmsg_t* respM=zmsg_new();
      zmsg_append(respM,&id);
      zmsg_addstr(respM,to_string(m).c_str()); //M
      zmsg_addstr(respM,to_string(o).c_str()); //O
      zmsg_print(respM);
      for (int i = 0; i <m; i++){
        zmsg_t* test=build[idC].top().second;
        for (int j = 0; j < o; j++){
            zframe_t* aaa=zmsg_pop(test);
            zmsg_append(respM,&aaa);
        }
        build[idC].pop();
      }
      cout<<"Mensaje a el cliente"<<endl;
      
      zmsg_send(&respM,clients);
      cout<<"Hola Recibidor"<<endl;
  }


}

void handleClientMessage(zmsg_t* msg, void* workers) {
  cout << "Handling the following message" << endl;
  zmsg_print(msg); 
  zframe_t* clientId = zmsg_pop(msg);
  
  decrip(msg);
  enviador(clientId,workers);
  cout << "End of handling" << endl;
  // zframe_destroy(&clientId);
  zmsg_destroy(&msg);
}  



void handleWorkerMessage(zmsg_t* msg, void* clients) {
  cout << "Handling the following WORKER" << endl;
  zmsg_print(msg);
  // Retrieve the identity and the operation code
  zframe_t* id = zmsg_pop(msg); // id worker
  char* opcode = zmsg_popstr(msg);
  if (strcmp(opcode, "register") == 0) {
    // Get the operation the worker computes
    //char* operation = zmsg_popstr(msg);
    // Register the worker in the server state
    registerWorker(id);
  } else if (strcmp(opcode, "answer") == 0) {
    //recibidor(msg,clients);
    zmsg_send(&msg,clients);
    //zmsg_send(&msg, clients);
  } else {
    cout << "Unhandled message" << endl;
  }
  cout << "End of handling" << endl;
  free(opcode);
  zframe_destroy(&id);
  zmsg_destroy(&msg);
}

int main(void) {
  zctx_t* context = zctx_new();
  // Socket to talk to the workers
  void* workers = zsocket_new(context, ZMQ_ROUTER);
  int workerPort = zsocket_bind(workers, "tcp://*:5555");
  cout << "Listen to workers at: "
       << "localhost:" << workerPort << endl;

  // Socket to talk to the clients
  void* clients = zsocket_new(context, ZMQ_ROUTER);
  int clientPort = zsocket_bind(clients, "tcp://*:4444");
  cout << "Listen to clients at: "
       << "localhost:" << clientPort << endl;

  zmq_pollitem_t items[] = {{workers, 0, ZMQ_POLLIN, 0},
                            {clients, 0, ZMQ_POLLIN, 0}};
  cout << "Listening!" << endl;

  while (true) {
    zmq_poll(items, 2, 10 * ZMQ_POLL_MSEC);
    if (items[0].revents & ZMQ_POLLIN) {
      cerr << "From workers\n";
      zmsg_t* msg = zmsg_recv(workers);
      cerr << "From workers2\n";
      handleWorkerMessage(msg,clients);

    }
    if (items[1].revents & ZMQ_POLLIN) {
      cerr << "From clients\n";
      zmsg_t* msg = zmsg_recv(clients);
      handleClientMessage(msg, workers);
    }
  }

  zctx_destroy(&context);
  return 0;
}
