/****************************************

****************************************/


#include <czmq.h>
#include <bits/stdc++.h>
#include <czmq.h>


using namespace std;
typedef vector<vector<int> > Matrix;
unordered_map<int,zmsg_t*> mensajes;

void file_to_msg(zmsg_t* msg,void* server){ // enviamos las 2 matrices 

  ifstream infile("A.txt");
  string line;

  while (getline(infile, line)){
  istringstream iss(line);
  int element;

    while (iss >> element){
      zmsg_addstr(msg,to_string(element).c_str());  
    }
  }
  infile.close();

  ifstream infile2("B.txt");
  string line2;

  while (getline(infile2, line2)){
  istringstream iss(line2);
  int element;

    while (iss >> element){
      zmsg_addstr(msg,to_string(element).c_str());  
    }
  }
  infile2.close();

  //zmsg_print(msg);
  
  /*if(myfile1.is_open())
  {
    while ( getline (myfile1,line) )
    {
      zmsg_addstr(msg,line.c_str());      
    }
    myfile1.close();
  }

  ifstream myfile2 ("B.txt");
  if(myfile2.is_open())
  {
    while ( getline (myfile2,line) )
    {
      zmsg_addstr(msg,line.c_str());      
    }
    myfile2.close();
  }
  //cout<<zmsg_size(msg)<<endl;

  //zframe_print(zmsg_pop(msg),"mi primer frame");
  //zmsg_print(msg);
  cont=0;*/
  zmsg_send(&msg,server);
}


void orden_en_la_sala(zmsg_t* msg){

  int m=atoi(zmsg_popstr(msg));
  int o=atoi(zmsg_popstr(msg));

  for (int i = 0; i < m; i++){
    for (int j = 0; j < o; j++){
      int element=atoi(zmsg_popstr(msg));
      cout<<element<<" ";
    }
    cout<<endl;
  }

}

void imprimir(int m,int o){
  /*
  for(const auto& entry : mensajes) {
    cout << " key " << entry.first << " -> " << entry.second << endl;
  }
  */
  for (int i = 0; i < mensajes.size();i++){
    zmsg_t* msg=zmsg_new();
    msg=mensajes[i];
    //zmsg_dump(msg);
    
    for(int j=0;j<o;j++){
      char * tmp = zmsg_popstr(msg);
      cout <<  tmp << " ";
      free(tmp);  
      //cout<<zmsg_popstr(msg)<<" ";
    }
    cout<<endl;
  }
 

}



int main(int argc, char** argv){

  zctx_t* context = zctx_new();
  void* server = zsocket_new(context, ZMQ_DEALER);
  zsocket_connect(server, "tcp://localhost:4444");  
  zmsg_t* msg = zmsg_new();
  file_to_msg(msg,server);


  cout << "Waiting for answer..." << endl;
  zmq_pollitem_t items[] = {{server, 0, ZMQ_POLLIN, 0}};

  while (true) {
    zmq_poll(items, 1, 10 * ZMQ_POLL_MSEC);
    if (items[0].revents & ZMQ_POLLIN) {
      zmsg_t* msg = zmsg_recv(server);
      //zmsg_dump(msg);
      int indice=atoi(zmsg_popstr(msg));
      int m=atoi(zmsg_popstr(msg));
      int o=atoi(zmsg_popstr(msg));
      mensajes[indice]=zmsg_dup(msg);
     // zmsg_dump(msg);
      //cout << "---Answer!!!:\n";

      if(mensajes.size()==m){
        cout << "Answer!!!:\n";
        cout<<"Matriz C: "<<endl;
        imprimir(m,o);
        break;
      }

   
      
    }
  }
  zctx_destroy(&context);
  return 0;
}
