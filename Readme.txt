**************************************************************
Multiplicadora de matrices cliente servidor modelo - división
filas por matriz.
**************************************************************

El programa se ejecutará de siguiente manera:

./server ./worker ./client

Para cambiar los valores de las matrices se debe modificar 
el contenido de los siguientes archivos: A.txt B.txt 

Poseerán el siguiente formato:


m= número de filas n= número de columnas

**************
m n
v1 v2 ...
......
...
**************
Ejemplo: 

Matriz A:

2 3 
5 1 2
4 5 10

Matriz B: 

3 5 
5 6 0 8 6
4 1 2 0 3 
7 7 8 9 7
